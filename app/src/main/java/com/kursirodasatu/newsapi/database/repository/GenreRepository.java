package com.kursirodasatu.newsapi.database.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.kursirodasatu.newsapi.database.dao.GenreDao;
import com.kursirodasatu.newsapi.database.room.RoomDatabases;
import com.kursirodasatu.newsapi.pojo.movie.Genre;

import java.util.List;

public class GenreRepository {
    private GenreDao mGenreDao;
    private LiveData<List<Genre.Genres>> mLiveData;

    public GenreRepository(Application application){
        RoomDatabases db = RoomDatabases.getDatabase(application);
        mGenreDao = db.genreDao();
        mLiveData = mGenreDao.getAllGenre();
    }

    public LiveData<List<Genre.Genres>> getAllGenres(){
        return mLiveData;
    }

    public void insertAll(List<Genre.Genres> genres){
        new insertAllAsyncTask(mGenreDao).execute(genres);
    }

    public void insertOne(Genre.Genres genres){
        new insertOneAsyncTask(mGenreDao).execute(genres);
    }

    private static class insertAllAsyncTask extends AsyncTask<List<Genre.Genres>, Void, Void>{

        private GenreDao mAsyncTaskDao;

        insertAllAsyncTask(GenreDao dao){
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(List<Genre.Genres>... lists) {
            mAsyncTaskDao.insertAll(lists[0]);
            return null;
        }
    }


    private static class insertOneAsyncTask extends AsyncTask<Genre.Genres, Void, Void>{

        private GenreDao mAsyncTaskDao;

        insertOneAsyncTask(GenreDao dao){
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(Genre.Genres... lists) {
            mAsyncTaskDao.insertOne(lists[0]);
            return null;
        }
    }
}
