package com.kursirodasatu.newsapi.pojo.movie;

import java.util.List;

public class DetailMovie {

    public int vote_count;
    public double vote_average;
    public boolean video;
    public String title;
    public String tagline;
    public String status;
    public List<Spoken_languages> spoken_languages;
    public int runtime;
    public int revenue;
    public String release_date;
    public List<Production_countries> production_countries;
    public List<Production_companies> production_companies;
    public String poster_path;
    public double popularity;
    public String overview;
    public String original_title;
    public String original_language;
    public String imdb_id;
    public int id;
    public String homepage;
    public List<Genres> genres;
    public int budget;
    public Belongs_to_collection belongs_to_collection;
    public String backdrop_path;
    public boolean adult;

    public static class Spoken_languages {
        public String name;
        public String iso_639_1;
    }

    public static class Production_countries {
        public String name;
        public String iso_3166_1;
    }

    public static class Production_companies {
        public String origin_country;
        public String name;
        public String logo_path;
        public int id;
    }

    public static class Genres {
        public String name;
        public int id;
    }

    public static class Belongs_to_collection {
        public String backdrop_path;
        public String poster_path;
        public String name;
        public int id;
    }
}
