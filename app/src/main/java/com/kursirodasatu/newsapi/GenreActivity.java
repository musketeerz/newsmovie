package com.kursirodasatu.newsapi;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.kursirodasatu.newsapi.movie.GenreFragment;
import com.kursirodasatu.newsapi.movie.MoviePageFragment;
import com.kursirodasatu.newsapi.setting.SettingPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GenreActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.appBar)
    AppBarLayout appBar;
    @BindView(R.id.viewPager)
    ViewPager viewPager;


    private SettingPagerAdapter settingPagerAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white);
        actionBar.setTitle("TheMovieDb.org");

        settingPagerAdapter = new SettingPagerAdapter(getSupportFragmentManager());
        setupViewPager(viewPager, new DetailFragment(), "Detail", 0);
        setupViewPager(viewPager, new GenreFragment(), "Genre", 1);
        setupViewPager(viewPager, new MoviePageFragment(), "Movie", 2);

        tabs.setupWithViewPager(viewPager);

        int limit = (settingPagerAdapter.getCount() > 1 ? settingPagerAdapter.getCount() - 1 : 1);
        viewPager.setOffscreenPageLimit(limit);

    }


    private void setupViewPager(ViewPager viewPager, Fragment fragment, String title, int position) {
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        settingPagerAdapter.addFragment(fragment, title);
        viewPager.setAdapter(settingPagerAdapter);
        viewPager.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
