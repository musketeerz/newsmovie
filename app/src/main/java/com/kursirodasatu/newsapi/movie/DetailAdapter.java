package com.kursirodasatu.newsapi.movie;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kursirodasatu.newsapi.R;
import com.kursirodasatu.newsapi.pojo.movie.Genre;

import java.util.List;
import java.util.Map;

public class DetailAdapter extends RecyclerView.Adapter<DetailHolder> {

    private Context context;
    private List<Map<String, String>> detail;


    public DetailAdapter(Context context) {
        this.context = context;
    }

    public void setDetail(List<Map<String, String>> detail) {
        this.detail = detail;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public DetailHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_movie_detail, parent, false);
        return new DetailHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DetailHolder holder, int position) {
        Map<String, String> item = detail.get(position);
        String key = item.keySet().toString().replaceAll("[\\[-\\]]+","");
        String value = item.values().toString().replaceAll("[\\[-\\]]+","");
        holder.tvDetailKey.setText(key);
        holder.tvDetailValue.setText(value);
    }

    @Override
    public int getItemCount() {
        if (detail != null){
            return detail.size();
        }else {
            return 0;
        }
    }
}
