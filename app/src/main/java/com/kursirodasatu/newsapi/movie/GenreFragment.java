package com.kursirodasatu.newsapi.movie;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kursirodasatu.newsapi.R;
import com.kursirodasatu.newsapi.database.viewmodel.GenreAndroidViewModel;
import com.kursirodasatu.newsapi.pojo.movie.Genre;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class GenreFragment extends Fragment {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_main_null, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        final GenreAdapter adapter = new GenreAdapter(getContext());

//        GenreApiViewModel model = ViewModelProviders.of(this).get(GenreApiViewModel.class);
//        model.getData().observe(getActivity(), new Observer<Genre>() {
//            @Override
//            public void onChanged(@Nullable Genre genre) {
//                adapter.setGenres(genre.genres);
//            }
//        });
        GenreAndroidViewModel genreAndroidViewModel = ViewModelProviders.of(this).get(GenreAndroidViewModel.class);
        genreAndroidViewModel.getAllGenres().observe(getActivity(), new Observer<List<Genre.Genres>>() {
            @Override
            public void onChanged(@Nullable List<Genre.Genres> genres) {
                adapter.setGenres(genres);
            }
        });
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
