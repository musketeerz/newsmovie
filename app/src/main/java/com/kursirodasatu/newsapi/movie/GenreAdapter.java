package com.kursirodasatu.newsapi.movie;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kursirodasatu.newsapi.R;
import com.kursirodasatu.newsapi.pojo.movie.Genre;

import java.util.List;

public class GenreAdapter extends RecyclerView.Adapter<GenreHolder> {

    private Context context;
    private List<Genre.Genres> genres;

    public GenreAdapter(Context context) {
        this.context = context;
    }

    public void setGenres(List<Genre.Genres> genres) {
        this.genres = genres;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public GenreHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_movie_genre, parent, false);
        return new GenreHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GenreHolder holder, int position) {
        Genre.Genres genre = genres.get(position);
        holder.tvGenreID.setText(String.valueOf(genre.id));
        holder.tvGenreName.setText(genre.name);
    }

    @Override
    public int getItemCount() {
        if (genres != null){
            return genres.size();
        }else {
            return 0;
        }
    }
}
