package com.kursirodasatu.newsapi.retrofit.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PageKeyedDataSource;
import android.arch.paging.PagedList;

import com.kursirodasatu.newsapi.pojo.article.Article;
import com.kursirodasatu.newsapi.retrofit.datasource.ArticlePageApiDataSourceFactory;
import com.kursirodasatu.newsapi.retrofit.datasource.ArticlePageApiDataSource;

public class ArticlePageApiViewModel extends ViewModel {

    private LiveData<PagedList<Article.Articles>> itemPagedList;
    private LiveData<PageKeyedDataSource<Integer, Article.Articles>> liveDataSource;
    private String question;

    public LiveData<PagedList<Article.Articles>> getPagedList(String question){
        this.question = question;
        if(itemPagedList == null){
            loadPagedList();
        }
        return itemPagedList;
    }

    private void loadPagedList(){
        ArticlePageApiDataSourceFactory articlePageApiDataSourceFactory = new ArticlePageApiDataSourceFactory(question);
        liveDataSource = articlePageApiDataSourceFactory.getItemLiveDataSource();
        PagedList.Config pagedListConfig =
                (new PagedList.Config.Builder())
                        .setEnablePlaceholders(false)
                        .setPageSize(ArticlePageApiDataSource.PAGE_SIZE)
                        .build();

        itemPagedList = (new LivePagedListBuilder(articlePageApiDataSourceFactory, pagedListConfig)).build();
    }
}
