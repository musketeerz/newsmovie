package com.kursirodasatu.newsapi.retrofit.datasource;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;
import android.arch.paging.PageKeyedDataSource;

import com.kursirodasatu.newsapi.pojo.article.Article;

public class ArticlePageApiDataSourceFactory extends DataSource.Factory {

    private MutableLiveData<PageKeyedDataSource<Integer, Article.Articles>> itemLiveDataSource = new MutableLiveData<>();
    private String question;



    public ArticlePageApiDataSourceFactory(String question) {
        this.question = question;
    }

    @Override
    public DataSource<Integer, Article.Articles> create() {
        ArticlePageApiDataSource articlePageApiDataSource = new ArticlePageApiDataSource(question);
        itemLiveDataSource.postValue(articlePageApiDataSource);

        return articlePageApiDataSource;
    }

    public MutableLiveData<PageKeyedDataSource<Integer, Article.Articles>> getItemLiveDataSource() {
        return itemLiveDataSource;
    }
}
