package com.kursirodasatu.newsapi.retrofit.api;

import com.kursirodasatu.newsapi.pojo.movie.DetailMovie;
import com.kursirodasatu.newsapi.pojo.movie.Genre;
import com.kursirodasatu.newsapi.pojo.movie.Movie;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiMovie {
    String BASE_URL = "https://api.themoviedb.org/3/";
    String API_KEY = "8199dd30388a445c4cd06e808f435538";

    @GET("genre/movie/list")
    Call<Genre> getAllGenre(
            @Query("api_key") String apiKey,
            @Query("language") String language
    );

    @GET("discover/movie")
    Call<Movie> getPageMovie(
            @Query("api_key") String apiKey,
            @Query("language") String language,
            @Query("sort_by") String sort_by,
            @Query("include_adult") boolean include_adult,
            @Query("include_video") boolean include_video,
            @Query("page") int page
    );

    @GET("movie/{id}")
    Call<DetailMovie> getDetailMovie(
            @Path("id") int id,
            @Query("api_key") String apiKey,
            @Query("language") String language
    );
}
