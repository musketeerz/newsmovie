package com.kursirodasatu.newsapi.retrofit.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import com.kursirodasatu.newsapi.pojo.movie.Genre;
import com.kursirodasatu.newsapi.retrofit.RetrofitClient;
import com.kursirodasatu.newsapi.retrofit.api.ApiMovie;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GenreApiViewModel extends ViewModel{
    private MutableLiveData<Genre> genres;

    public LiveData<Genre> getData(){
        if(genres == null){
            genres = new MutableLiveData<>();
            loadGenre();
        }
        return genres;
    }

    private void loadGenre(){
        RetrofitClient.getInstanceMovie()
                .getApiMovie()
                .getAllGenre(ApiMovie.API_KEY, "en-US")
                .enqueue(new Callback<Genre>() {
                    @Override
                    public void onResponse(@NonNull Call<Genre> call, Response<Genre> response) {
                        genres.setValue(response.body());
                    }

                    @Override
                    public void onFailure(@NonNull Call<Genre> call, Throwable t) {

                    }
                });
    }
}
