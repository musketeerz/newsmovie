package com.kursirodasatu.newsapi.retrofit.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PageKeyedDataSource;
import android.arch.paging.PagedList;

import com.kursirodasatu.newsapi.pojo.movie.Movie;
import com.kursirodasatu.newsapi.retrofit.datasource.MoviePageApiDataSource;
import com.kursirodasatu.newsapi.retrofit.datasource.MoviePageApiDataSourceFactory;

public class MoviePageApiViewModel extends ViewModel{
    private LiveData<PagedList<Movie.Results>> itemPagedList;
    private LiveData<PageKeyedDataSource<Integer, Movie.Results>> liveDataSource;

    public LiveData<PagedList<Movie.Results>> getItemPagedList() {
        if(itemPagedList == null){
            loadPagedList();
        }
        return itemPagedList;
    }

    private void loadPagedList(){
        MoviePageApiDataSourceFactory factory = new MoviePageApiDataSourceFactory();
        liveDataSource = factory.getMutableLiveData();
        PagedList.Config pagedListConfig =
                (new PagedList.Config.Builder())
                        .setEnablePlaceholders(false)
                        .setPageSize(MoviePageApiDataSource.PAGE_SIZE)
                        .build();

        itemPagedList = (new LivePagedListBuilder(factory, pagedListConfig)).build();
    }
}
