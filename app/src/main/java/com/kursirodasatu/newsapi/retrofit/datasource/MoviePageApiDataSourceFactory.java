package com.kursirodasatu.newsapi.retrofit.datasource;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;
import android.arch.paging.PageKeyedDataSource;

import com.kursirodasatu.newsapi.pojo.movie.Movie;

public class MoviePageApiDataSourceFactory extends DataSource.Factory<Integer, Movie.Results> {
    private MutableLiveData<PageKeyedDataSource<Integer, Movie.Results>> mutableLiveData = new MutableLiveData<>();

    public MutableLiveData<PageKeyedDataSource<Integer, Movie.Results>> getMutableLiveData() {
        return mutableLiveData;
    }


    @Override
    public DataSource<Integer, Movie.Results> create() {
        MoviePageApiDataSource moviePageApiDataSource = new MoviePageApiDataSource();
        mutableLiveData.postValue(moviePageApiDataSource);
        return moviePageApiDataSource;
    }
}
