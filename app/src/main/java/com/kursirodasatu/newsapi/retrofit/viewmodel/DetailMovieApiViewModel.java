package com.kursirodasatu.newsapi.retrofit.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import com.kursirodasatu.newsapi.pojo.movie.DetailMovie;
import com.kursirodasatu.newsapi.pojo.movie.Genre;
import com.kursirodasatu.newsapi.retrofit.RetrofitClient;
import com.kursirodasatu.newsapi.retrofit.api.ApiMovie;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailMovieApiViewModel extends ViewModel{
    private MutableLiveData<DetailMovie> detailMovieMutableLiveData;

    public LiveData<DetailMovie> getData(int id){
        if(detailMovieMutableLiveData == null){
            detailMovieMutableLiveData = new MutableLiveData<>();
            loadDetailMovie(id);
        }
        return detailMovieMutableLiveData;
    }

    private void loadDetailMovie(int id){
        RetrofitClient.getInstanceMovie()
                .getApiMovie()
                .getDetailMovie(id,ApiMovie.API_KEY, "en-US")
                .enqueue(new Callback<DetailMovie>() {
                    @Override
                    public void onResponse(Call<DetailMovie> call, Response<DetailMovie> response) {
                        detailMovieMutableLiveData.setValue(response.body());
                    }

                    @Override
                    public void onFailure(Call<DetailMovie> call, Throwable t) {

                    }
                });
    }
}
